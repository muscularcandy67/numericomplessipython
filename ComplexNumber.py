import math


class ComplexNumber:
    def __init__(self, rp, ip):
        self.r = rp
        self.i = ip

    def add(self, z):
        return ComplexNumber(self.r + z.r, self.i + z.i)

    def minus(self, z):
        return ComplexNumber(self.r - z.r, self.i - z.i)

    def conj(self):
        self.i = -self.i

    def neg(self):
        return ComplexNumber(self.r, -self.i)

    def mult(self, z):
        return ComplexNumber(self.r * z.r - self.i * z.i, self.r * z.i + self.i * z.r)

    def divn(self, z):
        p = self.r / z
        l = self.i / z
        print(p, l)

    def absSquared(self):
        return self.r * self.r + self.i * self.i

    def abss(self):
        return abs(self.absSquared())

    def inv(self):
        a = self.absSquared()
        return ComplexNumber(self.r / a, self.i / a)

    def divc(self, z):
        return self.mult(z.inv())

    def exp(self, z):
        return ComplexNumber(math.exp(self.r)*math.cos(self.i), math.exp(self.r)*math.sin(self.i))


x = ComplexNumber(3.0, -4.5)
print(x.r, x.i)
